#!/usr/bin/env ruby

require 'docopt'
require 'date'
require 'yaml'

docstring = <<DOCSTRING
Show historical stats on maintainers of GitLab

Usage:
  #{__FILE__}
  #{__FILE__} -h | --help

Options:
  -h --help                  Show this screen.
DOCSTRING

def show_maintainers_on(date)
  team_page_text = `git show $(git log --before=#{date} -1 --pretty='%H'):data/team.yml`
  team_page = YAML.load(team_page_text)

  maintainer_count =
    if team_page.first['maintains']
      # We used to call them endbosses.
      look_for = ['endboss']

      # Then we added frontend-specific endbosses.
      look_for << 'Backend merge request' if team_page.first['maintains'].include?('Backend merge request')

      team_page.select do |person|
        maintains = person['maintains']

        maintains && look_for.all? { |s| maintains.include?(s) }
      end.count
    else
      team_page.select { |person| person['projects'] && person['projects']['gitlab-ce'] == 'maintainer backend' }.count
    end

  developer_count = team_page.select do |person|
    person['gitlab'] && # Exclude vacancies
      (person['role'].match(/Developer[^<]*</) ||
       person['role'].include?('Backend Engineer') ||
       person['role'].include?('Engineering Fellow'))
  end.count

  row = [date, maintainer_count, developer_count, (developer_count.to_f / maintainer_count).round(1)]

  puts "| #{row.join(' | ')} |"
end

def show_maintainers
  current_date = Date.new(2016, 7, 1)
  end_date = Date.today

  puts '| Date | Maintainers | Developers | Developers per maintainer |'
  puts '| --- | --- | --- | --- |'

  loop do
    break if current_date > end_date

    show_maintainers_on(current_date)

    current_date = current_date >> 6
  end

  show_maintainers_on(end_date)
end

begin
  options = Docopt::docopt(docstring)

  show_maintainers
rescue Docopt::Exit => e
  puts e.message
end
